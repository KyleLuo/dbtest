# 工程简介

这个工程集成h2数据库进行测试数据库各方面的插件，
用来简单地测试各种依赖的性能

# 延伸阅读


[MyBatis-Plus](https://baomidou.com/guide/#%E7%89%B9%E6%80%A7)
[H2数据库](http://www.h2database.com/html/tutorial.html)
[druid](https://github.com/alibaba/druid)
