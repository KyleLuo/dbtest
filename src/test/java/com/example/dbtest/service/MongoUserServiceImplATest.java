package com.example.dbtest.service;

import cn.hutool.core.date.StopWatch;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.example.dbtest.entity.User;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MongoUserServiceImplATest {

    private static MongoTemplate mongoTemplate;

    @Autowired
    private MongoUserServiceImplA mongoUserServiceImplA;

    @Autowired
    private void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @BeforeEach
    private void init() {
        log.debug("如果存在表，则输出表。不存在则创建表");
        if (!mongoTemplate.collectionExists(User.class)) {
            MongoCollection<Document> document = mongoTemplate.createCollection(User.class, CollectionOptions.empty());
            mongoTemplate.indexOps(User.class).ensureIndex(new Index().on("name", Sort.Direction.ASC));
        } else {
            log.debug("表已经存在");
        }
    }

    @Test
    @Order(1)
    void insert() {
        List<User> list = new ArrayList<>(1000);
        for (int i = 0; i < 1000; i++) {
            User user = new User();
            user.setId(Long.valueOf(i));
            user.setAge(RandomUtil.randomInt());
            user.setName(RandomUtil.randomString(5));
            user.setEmail(RandomUtil.randomNumbers(10) + "@163.com");
            list.add(mongoUserServiceImplA.insert(user));
        }
        Assertions.assertEquals(1000, list.size());
    }

    @Test
    @Order(2)
    void deleteById() {
        List<DeleteResult> list = new ArrayList<>(500);
        for (int i = 500; i < 1000; i++) {
            list.add(mongoUserServiceImplA.deleteById(Long.valueOf(i)));
        }
        System.out.println(list.size());
        Assertions.assertEquals(500, list.size());
    }

    @Test
    @Order(3)
    void selectOne() {
        long id = RandomUtil.randomLong(0, 500);
        Query query = new Query(Criteria.where("_id").is(id));
        User user = mongoUserServiceImplA.selectOne(query);
        System.out.println(user);
        Assertions.assertNotNull(user);
    }

    @Test
    @Order(4)
    void selectList() {
        Query query = new Query();
        List<User> users = mongoUserServiceImplA.selectList(query);
        Assertions.assertEquals(500, users.size());
        users.stream().forEach(System.out::println);
    }

    @Test
    @Order(5)
    void updateById() {
        User user = new User();
        user.setId(1L);
        user.setAge(12);
        user.setName("kyle");
        user.setEmail("57777@qq.com");
        UpdateResult ur = mongoUserServiceImplA.updateById(user);
        System.out.println(ur);
        Assertions.assertEquals(1L, ur.getModifiedCount());
        Assertions.assertEquals(1L, ur.getMatchedCount());
    }

    /**
     * 1000000:5m 40s 482ms，一毫秒大约3条
     */
    @Test
    @Order(6)
    void insertBench() {
        final int dataSize = 100_0000;
        List<User> list = new ArrayList<>(dataSize);
        for (int i = 0; i < dataSize; i++) {
            User user = new User();
            user.setId(Long.valueOf(i));
            user.setAge(RandomUtil.randomInt());
            user.setName(RandomUtil.randomString(5));
            user.setEmail(RandomUtil.randomNumbers(10) + "@163.com");
            list.add(mongoUserServiceImplA.insert(user));
        }
        Assertions.assertEquals(dataSize, list.size());
    }

    /**
     * 100_0000: 22s 411ms
     */
    @Test
    @Order(7)
    void insertBatch() {
        final int dataSize = 200_0000;
        List<User> list = new ArrayList<>(dataSize);
        for (int i = 0; i < dataSize; i++) {
            User user = new User();
            user.setId(Long.valueOf(i));
            user.setAge(RandomUtil.randomInt());
            user.setName(RandomUtil.randomString(5));
            user.setEmail(RandomUtil.randomNumbers(10) + "@163.com");
            list.add(user);
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        BulkWriteResult res = mongoUserServiceImplA.insertBatch(list);
        stopWatch.stop();
        System.out.println("耗时" + stopWatch.prettyPrint());
        if(ObjectUtil.isNotNull(res)) {
            Assertions.assertEquals(dataSize, res.getMatchedCount());
        }
    }
}