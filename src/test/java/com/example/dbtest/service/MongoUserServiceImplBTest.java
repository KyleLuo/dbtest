package com.example.dbtest.service;

import cn.hutool.core.util.RandomUtil;
import com.example.dbtest.entity.User;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MongoUserServiceImplBTest {
    private static MongoTemplate mongoTemplate;

    @Autowired
    MongoUserServiceImplB mongoUserServiceImplB;

    @Autowired
    private void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @BeforeEach
    private void init() {
        log.debug("如果存在表，则输出表。不存在则创建表");
        if (!mongoTemplate.collectionExists(User.class)) {
            MongoCollection<Document> document = mongoTemplate.createCollection(User.class, CollectionOptions.empty());
            mongoTemplate.indexOps(User.class).ensureIndex(new Index().on("name", Sort.Direction.ASC));
        } else {
            log.debug("表已经存在");
        }
    }

    @Test
    @Order(1)
    void insert() {
        List<User> list = new ArrayList<>(1000);
        for (int i = 0; i < 1000; i++) {
            User user = new User();
            user.setId(Long.valueOf(i));
            user.setAge(RandomUtil.randomInt());
            user.setName(RandomUtil.randomString(5));
            user.setEmail(RandomUtil.randomNumbers(10) + "@163.com");
            list.add(mongoUserServiceImplB.insert(user));
        }
        Assertions.assertEquals(1000, list.size());
    }

    @Test
    @Order(2)
    void deleteById() {
        for (int i = 500; i < 1000; i++) {
            mongoUserServiceImplB.deleteById(Long.valueOf(i));
        }
    }

    @Test
    @Order(3)
    void selectOne() {
        User user = mongoUserServiceImplB.selectOne();
        System.out.println(user);
        Assertions.assertNotNull(user);
    }

    @Test
    @Order(4)
    void selectList() {
        List<User> users = mongoUserServiceImplB.selectList(null);
        Assertions.assertEquals(500, users.size());
        users.stream().forEach(System.out::println);
    }

    @Test
    void updateById() {
        User user=new User();
        user.setId(1L);
        user.setAge(12);
        user.setName("kyle");
        user.setEmail("57777@qq.com");
        User ur= mongoUserServiceImplB.updateById(user);
        System.out.println(ur);
        Assertions.assertEquals(1L,ur.getId());
        Assertions.assertEquals("kyle",ur.getName());
    }
}