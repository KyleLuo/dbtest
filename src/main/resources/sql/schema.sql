DROP TABLE IF EXISTS user;

CREATE TABLE user
(
    id BIGINT(20) NOT NULL COMMENT '主键ID',
    name VARCHAR(30) NULL DEFAULT NULL COMMENT '姓名',
    age INT(11) NULL DEFAULT NULL COMMENT '年龄',
    email VARCHAR(50) NULL DEFAULT NULL COMMENT '邮箱',
    PRIMARY KEY (id)
);


create table public.gar_logs
(
    id           bigserial not null
        constraint _copy_12
        primary key,
    user_id      bigint,
    username     varchar(128),
    app_id       bigint,
    appname      varchar(128),
    operation    varchar(128),
    remark       varchar(2000),
    created_time timestamp(6),
    updated_time timestamp(6),
    status       smallint,
    ip           varchar(128),
    remark1      varchar(255),
    remark2      varchar(255)
);

create
index created_time
	on public.gar_logs (created_time);

create
index updated_time
	on public.gar_logs (updated_time);

create
index user_id
	on public.gar_logs (user_id);

create
index username
	on public.gar_logs (username);

