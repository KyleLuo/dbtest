package com.example.dbtest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dbtest.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
