package com.example.dbtest.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document
public class User {
    @MongoId(FieldType.INT64)
    private Long id;
    /**
     * Spring Data MongoDB3.X 后的版本不再支持自动创建索引，
     * 如果要坚持使用@Indexed注解需要额外的设置，并建议我们在手动创建索引
     */
    @Indexed(useGeneratedName = true)
    private String name;
    private Integer age;
    private String email;
}
