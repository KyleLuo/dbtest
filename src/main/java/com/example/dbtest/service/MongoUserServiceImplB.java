package com.example.dbtest.service;

import cn.hutool.core.util.RandomUtil;
import com.example.dbtest.entity.User;
import com.example.dbtest.repository.UserRepository;
import com.mongodb.bulk.BulkWriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongoUserServiceImplB {
    private final UserRepository userRepository;

    @Autowired
    public MongoUserServiceImplB(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User insert(User entity) {
        return userRepository.save(entity);
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    public User selectOne() {
        User user = new User();
        user.setId(RandomUtil.randomLong(0, 500));
        Example<User> example = Example.of(user);
        return userRepository.findOne(example).orElse(user);
    }

    public List<User> insertBatch(List<User> entity) {
        return userRepository.saveAll(entity);
    }

    public List<User> selectList(Query queryWrapper) {
        return userRepository.findAll();
    }

    public User updateById(User entity) {
        return userRepository.save(entity);
    }
}
