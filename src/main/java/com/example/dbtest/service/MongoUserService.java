package com.example.dbtest.service;

import com.example.dbtest.entity.User;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface MongoUserService {
    User insert(User entity);

    DeleteResult deleteById(Long id);

    User selectOne(Query queryWrapper);

    List<User> selectList(Query queryWrapper);

    UpdateResult updateById(User entity);
}
