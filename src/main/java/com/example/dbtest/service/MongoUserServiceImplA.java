package com.example.dbtest.service;

import com.example.dbtest.entity.User;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Slf4j
@Component
public class MongoUserServiceImplA {
    private final MongoTemplate mongoTemplate;

    @Autowired
    public MongoUserServiceImplA(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    /**
     * 增加
     *
     * @param entity
     * @return
     */
    public User insert(User entity) {
        return mongoTemplate.insert(entity);
    }

    /**
     * 保存
     *
     * @param entity
     * @return
     */
    public User save(User entity) {
        return mongoTemplate.save(entity);
    }

    /**
     * 批量插入，UNORDERED可以忽略重复id插入错误
     *
     * @param entity
     * @return
     */
    public BulkWriteResult insertBatch(List<User> entity) {
        BulkOperations bulkOp = this.mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, User.class);

        entity.forEach(bulkOp::insert);
        try {
            return bulkOp.execute();
        } catch (Exception ignored) {
            log.error("忽略插入不成功的插入，没有id冲突的将插入成功");
        }
        return null;
    }

    /**
     * 批量插入，不可以忽略重复id插入错误
     *
     * @param entity
     * @return
     */
    public Collection<User> insertBatch2(List<User> entity) {
        return mongoTemplate.insertAll(entity);
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    public DeleteResult deleteById(Long id) {
        Query query = new Query(Criteria.where("_id").is(id));
        return mongoTemplate.remove(query, User.class);
    }

    /**
     * 查出一个数据
     * @param queryWrapper
     * @return
     */
    public User selectOne(Query queryWrapper) {
        return mongoTemplate.findOne(queryWrapper, User.class);
    }

    /**
     * 查出一堆数据
     * @param queryWrapper
     * @return
     */
    public List<User> selectList(Query queryWrapper) {
        return mongoTemplate.find(queryWrapper, User.class);
    }

    /**
     * 通过id更新数据
     * @param entity
     * @return
     */
    public UpdateResult updateById(User entity) {
        Query query = new Query(Criteria.where("id").is(entity.getId()));
        Update update = new Update().set("age", entity.getAge()).set("name", entity.getName());
        //更新查询返回结果集的第一条
        return mongoTemplate.updateFirst(query, update, User.class);
        //更新查询返回结果集的所有
        // mongoTemplate.updateMulti(query,update,TestEntity.class);
    }
}
